# typescript-101
Introduction to typescript workshop

## Part 1: Frontend
### Exercise one:
1. Add typescript support for your workspace
2. Change the ext of App.js and App.test.js to `tsx`
3. Fix any errors you may encounter to run your TypeScript project

### Exercise two:
Create an ImageViewer component with the following properties:
- `images` - an array of allowed (images) urls and optional titles. Allows:
    + `autoplay` - can define the delay (with a default value of no autoplay). Allows:
        * `direction` - “forward” or “back” (with a default value)
- `staticImage` - not allowed with images
- `fit` - determines image fit in containing element 

### Exercise three:
Improvements to ImageViewer
1. Make sure the state strongly typed
2. Use generics, type intersection/union to express:
    - The Images array can't be used with staticImage
    - The image Urls must be valid
3. Make your types smaller more reusable
6. Just for fun, set some invalid url values
    - How can you prevent that kind of abuse?
    - What are the costs? When should you do it? 

## Part 2: Backend
### Exercise four: Server setup
1. Create a backend package
2. Add typescript, express, sequelize, pg & lodash
3. Add @types/node, @types/express, @types/sequelize 
4. Run yarn tsc --init
5. Add package.json scripts: build, start and start:dev
6. Write a hello world endpoint

### Exercise five: Cats Overload Server
1. Create a server with the following endpoint:
    - /suggest/:count - Get a list of random images from `thecatapi.com`
2. Using lodash **without importing the types** to filter images by format and sort them by size
3. Modify the response to your own model

### Exercise six: Front and back
Update UI to use the server:
1. Move the model to its own module & sharing DTO with the backend
2. Load a suggestion, list, load and save collections
3. Have a splash screen while loading data (using staticImage)
4. Add a delete image button with kill kitten animation

## Part 3: Getting serious
### Exercise seven: Getting real
Use your new skills on a real project:
1. Choose a production module you know well
2. Select a commonly used function/class. Make sure it has external (i.e. imported) dependencies
3. Extract it to a TS file and add types as needed
4. Make sure your project builds and passed tests
5. Select another commonly used module
6. Add API types without changing the code (i.e. add a .t.ds file)
7. Repeat for modules that are imported by a other packages
